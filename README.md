# dmgpolden

**Symfony**

1. Создание word документа на основе шаблона 

https://gitlab.com/dmgpolden/dmgpolden/-/blob/master/symfony/Controller/ContractController.php#L103

2. Сбор статистики на основе загруженных данных из файлов для консоли и приложения 

 https://gitlab.com/dmgpolden/dmgpolden/-/blob/master/symfony/Service/StatisticService.php#L21

View и Triggers для статистики

3. Сервис корретировки балансов водителей

 https://gitlab.com/dmgpolden/dmgpolden/-/blob/master/symfony/Service/BalanceCorrector.php#L7

4. Обработка в фоне большого количества данных 
- Обработчик

https://gitlab.com/dmgpolden/dmgpolden/-/blob/master/symfony/MessageHandler/ProcessFileWeeklyHandler.php#L16
- Класс для постановки в очередь 

https://gitlab.com/dmgpolden/dmgpolden/-/blob/master/symfony/Message/ProcessFileWeekly.php#L5

- websocket для информирования клиента о ходе загрузки

https://gitlab.com/dmgpolden/dmgpolden/-/blob/master/symfony/Websocket/ImportWeekMessageHandler.php#L12

-  Демоны SystemD

https://gitlab.com/dmgpolden/dmgpolden/-/tree/master/symfony/systemd

5. Небольшой домашний проект для себя 

https://gitlab.com/dmgpolden/animalmotel


**Yii2**

1. Водяныe знаки со статусом с выбором местоположения. 

https://gitlab.com/dmgpolden/minihuskyRu/-/blob/master/common/modules/page/handlers/StatusHandler.php#L22

2. Обработчик картинок для п. 1

https://gitlab.com/dmgpolden/minihuskyRu/-/blob/master/common/components/ImageWatermark.php#L22


* Настройки nginx для изменения размера изображений в зависимости от url

https://gitlab.com/dmgpolden/minihuskyRu/-/blob/master/environments/dev/config/minihuskyRu.nginx.conf

* Сайты на yii2 с bootsrap-css
http://yourborzoi.com/
https://minihusky.ru/
