<?php

namespace App\MessageHandler;

use App\Entity\BillingTransaction;
use App\Entity\BillingWeekly;
use App\Entity\RegistrationEntry;
use App\Message\ProcessFileWeekly;
use Datetime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use SymfonyBundles\RedisBundle\Redis\ClientInterface as Redis;

class ProcessFileWeeklyHandler implements MessageHandlerInterface
{
    private $logger;
    private $em;
    private $router;

    public function __construct(EntityManagerInterface $em, LoggerInterface $logger, UrlGeneratorInterface $router, Redis $redis)
    {
        $this->em = $em;
        $this->logger = $logger;
        $this->router = $router;
        $this->redis = $redis;
    }

    public function __invoke(ProcessFileWeekly $message)
    {
        $reportOverallItem = $message->getContent();
        $direct = $message->getDirect();
        $reportFileName = $message->getReportFileName();
        $originalReportFileName = $message->getOriginalReportFileName();
        $userEmail = $message->getUserEmail();
        $weeklyCorrect = 0;
        $phone = str_replace('+374', '', str_replace('+7', '', $reportOverallItem[1]));

        $keyCount = 'countWeek';
        $keyCount .= $direct ? 'Direct' : 'Urban';
        $this->redis->lpush($keyCount, serialize($reportOverallItem));

        //var_dump($reportOverallItem[1]);
        if (empty($reportOverallItem[1])) {
            return;
        }
        if ('Վարորդի հեռախոսահամար' == $reportOverallItem[1]) {
            return;
        }

        $weeklyCorrect = floatval($reportOverallItem[count($reportOverallItem) - 1]);

        /** @var RegistrationEntry $registrationEntry */
        $registrationEntry = $this->em->getRepository(RegistrationEntry::class)->findOneBy([
            'phone' => $phone,
        ]);
        if (!$registrationEntry && !$direct) {
            $keyENE = 'registrationEntriesNonExisting';
            $keyENE .= $direct ? 'Direct' : 'Urban';
            $this->redis->lpush($keyENE, serialize($reportOverallItem));
        }

        $dateBeginWeekly = DateTime::createFromFormat(
            'Y-m-d H:i:s',
            explode(' ', trim($reportOverallItem[2]))[1].' 00:00:00'
        );
        $dateEndWeekly = DateTime::createFromFormat(
            'Y-m-d H:i:s',
            explode(' ', trim($reportOverallItem[2]))[3].' 23:59:59'
        );
        if (!$dateBeginWeekly) {
            throw new \Exception('Unable to parse date '.explode(' ', trim($reportOverallItem[2]))[1]);
        }

        if (!$dateEndWeekly) {
            throw new \Exception('Unable to parse date '.explode(' ', trim($reportOverallItem[2]))[3]);
        }

        if (!$registrationEntry) {
            $registrationEntry = new RegistrationEntry();
            $registrationEntry->setPhone($phone);
            $registrationEntry->setSms(rand(1001, 9999));
            $registrationEntry->setCreatedAt(new DateTime('now'));
            $registrationEntry->setAt(new DateTime('now'));
            $registrationEntry->setYesno(true);
            $registrationEntry->setChecked(true);
            $registrationEntry->setCardEnabled(false);
            $registrationEntry->setType($direct ? RegistrationEntry::TYPE_DIRECT : RegistrationEntry::TYPE_URBAN);
            $nameF = $reportOverallItem[0];
            $nameL = $reportOverallItem[0];

            try {
                $nameF = explode(' ', $reportOverallItem[0])[0];
                $nameL = explode(' ', $reportOverallItem[0])[1];
            } catch (\Exception $e) {
            }
            $registrationEntry->setDriverNameFirst($nameF);
            $registrationEntry->setDriverNameLast($nameL);
            $registrationEntry->setToken(uniqid().'_'.uniqid().'_'.uniqid());
            $registrationEntry->addToChronology('Added from import '.$originalReportFileName);
        }
        $registrationEntry->setYesno(true);
        $registrationEntry->setChecked(true);
        $registrationEntry->setType($direct ? RegistrationEntry::TYPE_DIRECT : RegistrationEntry::TYPE_URBAN);

        $testReport = $reportOverallItem;
        array_shift($testReport);
        array_shift($testReport);
        array_shift($testReport);
        $_tAr = [];
        foreach ($testReport as $_t) {
            if (0 == $_t) {
                array_push($_tAr, $_t);
            }
        }
        if (count($testReport) == count($_tAr)) {
            $keyEWZ = 'registrationEntriesWeekZero';
            $keyEWZ .= $direct ? 'Direct' : 'Urban';
            $this->redis->lpush($keyEWZ, "$phone skipped, zero sum gived");

            return;
        }
        $balanceBeginWeek = floatval(0);
        $btr = $this->em->getRepository(BillingTransaction::class);
        //Find last transaction frome week before
        // Else count balance
        $billingTransaction = $btr->findEndForDay($registrationEntry, $dateBeginWeekly);
        if ($billingTransaction && null !== $billingTransaction->getBalanceAfter()) {
            $balanceBeginWeek = floatval($billingTransaction->getBalanceAfter());
        }

        $balanceEndWeek = floatval(0);
        $btr = $this->em->getRepository(BillingTransaction::class);
        //Поиск балансов без учёта еженедельной корректировки
        //сортировка at, id

        $billingTransaction = $btr->findEndForDay(
            $registrationEntry,
            $dateEndWeekly,
            [
                ['field' => 'provider',
                    'operator' => 'NOT IN',
                    'parameter' => [BillingTransaction::PROVIDER_WEEKLY],
                ], ]
        );

        if ($billingTransaction && null !== $billingTransaction->getBalanceAfter()) {
            $balanceEndWeek = floatval($billingTransaction->getBalanceAfter());
            $dateEndWeekly = $billingTransaction->getAt();
        }

        $dateNow = new DateTime('now');
        $sums = $btr->findSumFromInterval($registrationEntry, $dateBeginWeekly, $dateEndWeekly);
        $sumIn = $sumOut = 0;
        if (!empty($sums)) {
            foreach ($sums as $sum) {
                switch ($sum['type']) {
                       case 'IN':
                       $sumIn = $sum['sum'];

                       break;
                       case 'OUT':
                       $sumOut = $sum['sum'];

                       break;
                    }
            }
        }
        // dump($sumIn, $sumOut);
        //  continue; // temp
        $dBw = $dateBeginWeekly->format('d-m-Y');
        $dEw = $dateEndWeekly->format('d-m-Y');
        $ph = $registrationEntry->getPhone();
        $newBalanceEndWeek = $weeklyCorrect + $balanceBeginWeek + $sumIn - $sumOut;
        $correctSum = $newBalanceEndWeek - $balanceEndWeek;

        $this->em->getConnection()->beginTransaction();

        try {
            $billingWeekly = new BillingWeekly();
            $billingWeekly->setEntry($registrationEntry);
            $billingWeekly->setAmount($correctSum);
            $billingWeekly->setImportedBy($userEmail);
            $billingWeekly->setAt(new \DateTime('now'));
            $billingWeekly->setInvalidated(false);
            $billingWeekly->setDate($dateEndWeekly);
            $billingWeekly->setRowData(implode(',', $reportOverallItem));
            $billingWeekly->setReportFile($reportFileName);

            $alreadyAddedBillingWeekly = $this->em->getRepository(BillingWeekly::class)->findBy([
                'entry' => $registrationEntry,
                'date' => $dateEndWeekly,
                'invalidated' => false,
            ], [
                'at' => 'DESC',
            ], 1);

            if (1 == sizeof($alreadyAddedBillingWeekly)) {
                $alreadyAddedBillingWeekly = $alreadyAddedBillingWeekly[0];
            } else {
                $alreadyAddedBillingWeekly = null;
            }

            /** @var BillingDaily $alreadyAddedBillingDaily */
            if ($alreadyAddedBillingWeekly) {
                $alreadyAddedBillingWeekly->setInvalidated(true);
                $this->em->persist($alreadyAddedBillingWeekly);

                $billingTransaction = new BillingTransaction();
                $billingTransaction->setEntry($registrationEntry);
                $billingTransaction->setProvider(BillingTransaction::PROVIDER_WEEKLY);
                $billingTransaction->setType($alreadyAddedBillingWeekly->getAmount() > 0 ? BillingTransaction::TYPE_CHECKOUT : BillingTransaction::TYPE_CHECKIN);
                $billingTransaction->setSuccess(true);
                $billingTransaction->setCreatedBy($userEmail);
                $billingTransaction->setAt($dateEndWeekly->modify('+1 millisecond'));
                $billingTransaction->setAmount(0 - $alreadyAddedBillingWeekly->getAmount());
                $billingTransaction->setRemoteData(
                    'Revert at '.(new \DateTime('now'))->format('d.m.Y H:i:s').' sum :'.$billingTransaction->getAmount()
                );
                $lastWeekTransaction = $this->em->getRepository(\App\Entity\BillingTransaction::class)
                                           ->findEndForDay($registrationEntry, $dateEndWeekly, [
                                               ['field' => 'provider',
                                                   'operator' => 'IN',
                                                   'parameter' => [BillingTransaction::PROVIDER_WEEKLY],
                                               ], ]);
                $balance = null == $lastWeekTransaction ? 0 : $lastWeekTransaction->getBalanceAfter();
                $billingTransaction->setBalanceBefore($balance);
                $registrationEntry->setBalance($registrationEntry->getBalance() - $alreadyAddedBillingWeekly->getAmount());
                $balance -= $alreadyAddedBillingWeekly->getAmount();
                $billingTransaction->setBalanceAfter($balance);
                $lastTransactions = $this->em->getRepository(\App\Entity\BillingTransaction::class)->findFromDateAfter($registrationEntry, $dateEndWeekly);
                foreach ($lastTransactions as $lTrans) {
                    $lTrans->setBalanceAfter($lTrans->getBalanceAfter() - $alreadyAddedBillingWeekly->getAmount());
                    $lTrans->setBalanceBefore($lTrans->getBalanceBefore() - $alreadyAddedBillingWeekly->getAmount());
                    $this->em->persist($lTrans);
                }

                // dd($billingTransaction);
                $registrationEntry->addToChronology("Removing previous balance {$alreadyAddedBillingWeekly->getAmount()} based weekly correct row {$alreadyAddedBillingWeekly->getId()}");
                $this->em->persist($billingTransaction);
            }

            $billingTransaction = new BillingTransaction();
            $billingTransaction->setEntry($registrationEntry);
            $billingTransaction->setProvider(BillingTransaction::PROVIDER_WEEKLY);
            $billingTransaction->setType($correctSum < 0 ? BillingTransaction::TYPE_CHECKOUT : BillingTransaction::TYPE_CHECKIN);
            $billingTransaction->setSuccess(true);
            $billingTransaction->setCreatedBy($userEmail);
            $billingTransaction->setAt($dateEndWeekly->modify('+1 millisecond'));
            $billingTransaction->setAmount($billingWeekly->getAmount());
            $billingTransaction->setRemoteData(
                'Import at '.(new \DateTime('now'))->format('d.m.Y H:i:s').' sum :'.$weeklyCorrect
            );
            $balance = $balanceEndWeek;
            $billingTransaction->setBalanceBefore($balance);
            switch ($billingTransaction->getType()) {
                            case \App\Entity\BillingTransaction::TYPE_CHECKOUT:
                                $balance -= abs(floatval($billingTransaction->getAmount()));
                                $registrationEntry->setBalance($registrationEntry->getBalance() - abs(floatval($correctSum)));

                                break;
                            case \App\Entity\BillingTransaction::TYPE_CHECKIN:
                                $balance += abs(floatval($billingTransaction->getAmount()));
                                $registrationEntry->setBalance($registrationEntry->getBalance() + abs(floatval($correctSum)));

                                break;
                        }

            $billingTransaction->setBalanceAfter($balance);
            $this->em->persist($billingTransaction);

//                $registrationEntry->applyTransaction($billingTransaction);
//                $registrationEntry->addToChronology("Adding to balance {$billingWeekly->getAmount()} based weekly correct row {$billingWeekly->getId()}");
            $lastTransactions = $this->em->getRepository(\App\Entity\BillingTransaction::class)->findFromDateAfter($registrationEntry, $dateEndWeekly);
            foreach ($lastTransactions as $lTrans) {
                switch ($billingTransaction->getType()) {
                                case \App\Entity\BillingTransaction::TYPE_CHECKOUT:
                                    $lTrans->setBalanceAfter($lTrans->getBalanceAfter() - abs($billingWeekly->getAmount()));
                                    $lTrans->setBalanceBefore($lTrans->getBalanceBefore() - abs($billingWeekly->getAmount()));

                                    break;
                                case \App\Entity\BillingTransaction::TYPE_CHECKIN:
                                    $lTrans->setBalanceAfter($lTrans->getBalanceAfter() + abs($billingWeekly->getAmount()));
                                    $lTrans->setBalanceBefore($lTrans->getBalanceBefore() + abs($billingWeekly->getAmount()));

                                    break;
                            }
                $this->em->persist($lTrans);
            }

            $url = $this->router->generate(
                'easyadmin',
                [
                    'action' => 'search',
                    'entity' => 'Transaction',
                    'query' => $registrationEntry->getPhone(),
                ]
            );

            $reportWeekly = '<a href="'.$url.'" target="_blank">'.$ph.'</a>: '."$dBw-$dEw week: $weeklyCorrect, begin: $balanceBeginWeek, in :$sumIn, out: $sumOut, end: $balanceEndWeek, new weekly Balance: $newBalanceEndWeek;  now balance ={$registrationEntry->getBalance()}";
            $keyEWW = 'registrationEntriesWeekWorker';
            $keyEWW .= $direct ? 'Direct' : 'Urban';
            $this->redis->lpush($keyEWW, $reportWeekly);
            $billingWeekly->setReport($reportWeekly);
            $this->em->persist($billingWeekly);

            $this->em->persist($registrationEntry);
            $this->em->flush();
            $this->em->getConnection()->commit();
        } catch (\Exception $e) {
            $this->em->getConnection()->rollBack();
            $keyENI = 'registrationEntriesNonInserted';
            $keyENI .= $direct ? 'Direct' : 'Urban';
            $this->redis->lpush($keyENI, $registrationEntry);

            throw new \Exception($e);
        }
    }
}
