<?php

namespace App\Message;

class ProcessFileWeekly
{
    private $content;
    private $direct;
    private $userEmail;
    private $reportFileName;
    private $originalReportFileName;

    public function __construct(array $content, $direct, $userEmail, $reportFileName, $originalReportFileName)
    {
        $this->content = $content;
        $this->direct = $direct;
        $this->reportFileName = $reportFileName;
        $this->originalReportFileName = $originalReportFileName;
        $this->userEmail = $userEmail;
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function getReportFileName()
    {
        return $this->reportFileName;
    }

    public function getOriginalReportFileName()
    {
        return $this->originalReportFileName;
    }

    public function getDirect()
    {
        return $this->direct;
    }

    public function getUserEmail()
    {
        return $this->userEmail;
    }
}
