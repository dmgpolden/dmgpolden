<?php

namespace App\Websocket;

use Doctrine\Bundle\DoctrineBundle\Registry as Doctrine;
use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use SplObjectStorage;
use Symfony\Component\Messenger\HandleTrait;

class ImportWeekMessageHandler implements MessageComponentInterface
{
    use HandleTrait;

    protected $connections;
    protected $countI;
    protected $doctrine;
    private $redis;

    public function __construct(Doctrine $doctrine, $redis)
    {
        $this->connections = new SplObjectStorage();
        $this->countI = 0;
        $this->doctrine = $doctrine;
        $this->redis = $redis;
        $redis->getProfile()->defineCommand('xlen', XlenRedisCommand::class);
    }

    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections->attach($conn);
    }

    public function onSubscribe(ConnectionInterface $connection, $subscription)
    {
        /**
         * Определяем количество необработанных сообщений
         */
        $countUrban = (int) $this->redis->llen('countWeekUrban');
        $countDirect = (int) $this->redis->llen('countWeekDirect');
//        dump($countUrban, $countDirect);
        //$countUrban = $this->doctrine->getConnection()->query('select count(*) From messenger_messages WHERE queue_name=\'importWeekUrban\'')->fetch()['count'];
        //$countDirect = $this->doctrine->getConnection()->query('select count(*) From messenger_messages WHERE queue_name=\'importWeekDirect\'')->fetch()['count'];
        
        /**
         * Отправляем количество сообщений клиенту
         */
        switch ($subscription) {
            case 'importWeekUrban':
        $msg['channel'] = 'importWeekUrban';
        $msg['count'] = $countUrban;
        $connection->send(json_encode($msg));
            break;
            case 'importWeekDirect':
        $msg['channel'] = 'importWeekDirect';
        $msg['count'] = $countDirect;
        $connection->send(json_encode($msg));
            break;
            default:
        }
        
        /*
         * Что делаем когда сообщения закнчились?
         */
        if (0 == $countUrban && 0 == $countDirect) {
        //    $connection->send(json_encode(['result' => true]));
        //    sleep(1);
        //$connection->close();
        } else {
            //    sleep(1);
        }
        /*
        
        KEYS *
        1) "registrationEntriesWeekWorkerDirect"
        2) "registrationEntriesWeekWorkerUrban"
        3) "registrationEntriesWeekZeroDirect"
        4) "registrationEntriesWeekZeroUrban"
        5) "countWeekUrban"
        6) "importWeek"
        7) "countWeekDirect"
        
        */
    }

    public function onMessage(ConnectionInterface $connection, $msg)
    {
        $msg = json_decode($msg, true);
        if (isset($msg['command']) && 'subscribe' == $msg['command']) {
            $this->onSubscribe($connection, $msg['channel']);
        }
        $connection->send(json_encode($msg));
    }

    public function onClose(ConnectionInterface $conn)
    {
        $this->connections->detach($conn);
    }

    public function onError(ConnectionInterface $conn, Exception $e)
    {
        $this->connections->detach($conn);
        //   $conn->close();
    }
}
