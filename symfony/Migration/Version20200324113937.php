<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200324113937 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('
                    create or replace view statistic_daily_view as
                    select
                    us.id as id,
                    us.date as date,
                    us.count_orders as urban_count_orders,
                    ds.count_orders as direct_count_orders,
                    us.count_orders + ds.count_orders as all_count_orders,
                    us.count_order_drivers as urban_core,
                    ds.count_order_drivers as direct_core,
                    us.count_order_drivers + ds.count_order_drivers as all_core,
                    us.sum_orders as urban_sum_orders,
                    ds.sum_orders as direct_sum_orders,
                    us.sum_orders + ds.sum_orders as all_sum_orders,
                    us.sum_drivers as urban_sum_drivers,
                    ds.sum_drivers as direct_sum_drivers,
                    us.sum_drivers + ds.sum_drivers as all_sum_drivers,
                    us.count_drivers as urban_count_drivers,
                    ds.count_drivers as direct_count_drivers,
                    us.count_drivers + ds.count_drivers as all_count_drivers,
                    us.comission_sum as urban_comission_sum,
                    ds.comission_sum as direct_comission_sum,
                    us.comission_sum + ds.comission_sum as all_comission_sum,
                    ( us.count_orders + ds.count_orders )::float / ( us.count_order_drivers + ds.count_order_drivers ) as count_orders_devide_core,
                    ( us.sum_orders + ds.sum_orders ) / ( us.count_orders + ds.count_orders ) as sum_orders_devide_count_orders
                    from statistic us
                    left join statistic ds on us.date=ds.date and ds.direct=true
                    where us.direct = false
                        ');

        $this->addSql('
                    create or replace view statistic_week_view as
                    select
                    us.id as id,
                    ds.id as did,
                    us.date_begin as date_begin,
                    us.date_end as date_end,
                    us.sum_pays as urban_sum_pays,
                    ds.sum_pays as direct_sum_pays,
                    us.sum_pays + ds.sum_pays as all_sum_pays,
                    us.sum_bonuses as urban_sum_bonuses,
                    ds.sum_bonuses as direct_sum_bonuses,
                    us.sum_bonuses + ds.sum_bonuses as all_sum_bonuses,
                    us.sum_orders as urban_sum_orders,
                    ds.sum_orders as direct_sum_orders,
                    us.sum_orders + ds.sum_orders as all_sum_orders,
                    us.sum_drivers as urban_sum_drivers,
                    ds.sum_drivers as direct_sum_drivers,
                    us.sum_drivers + ds.sum_drivers as all_sum_drivers,
                    us.comission_sum as urban_comission_sum,
                    ds.comission_sum as direct_comission_sum,
                    us.comission_sum + ds.comission_sum as all_comission_sum,
                    us.sum_z as urban_sum_z,
                    ds.sum_z as direct_sum_z,
                    us.sum_z + ds.sum_z as all_sum_z,
                    us.sum_to_pays as urban_sum_to_pays,
                    ds.sum_to_pays as direct_sum_to_pays,
                    us.sum_to_pays + ds.sum_to_pays as all_sum_to_pays
                    from statistic_week us
                    left join statistic_week ds on us.date_begin=ds.date_begin and ds.direct=true
                    where us.direct = false
                        ');
        $this->addSql('
            CREATE RULE statistic_week_view_INSTEAD AS ON UPDATE TO statistic_week_view DO INSTEAD (
               UPDATE statistic_week SET sum_pays=NEW.urban_sum_pays WHERE id=OLD.id;
               UPDATE statistic_week SET sum_pays=NEW.direct_sum_pays WHERE id=OLD.did;
                    );
                        ');
        $this->addSql('
            CREATE OR REPLACE FUNCTION setSumPays() RETURNS TRIGGER AS $$
                DECLARE 
                    oldComission double precision;
                    oldSumZ double precision;
            BEGIN
                    oldComission=0;
                    oldSumZ=0;
                SELECT comission_sum, sum_z INTO oldComission, oldSumZ
                    FROM statistic_week
                    WHERE direct = NEW.direct AND date_end=NEW.date_begin - integer \'1\';
                IF oldComission IS NOT NULL
                    THEN 
                         NEW.sum_to_pays := NEW.sum_pays - oldComission - oldSumZ;
                    END IF;
                RETURN NEW;
            END
            $$ LANGUAGE \'plpgsql\';
                        ');

        $this->addSql('DROP TRIGGER IF EXISTS statistic_week_sum_to_pay ON statistic_week ;');
        $this->addSql('
            CREATE TRIGGER statistic_week_sum_to_pay BEFORE UPDATE
                ON statistic_week FOR ROW
                EXECUTE PROCEDURE setSumPays();
                        ');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('drop view statistic_daily_view');
        $this->addSql('drop view statistic_week_view');
    }
}
