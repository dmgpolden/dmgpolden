<?php

namespace App\Controller;

use App\Entity\Contract;
use App\Entity\RegistrationEntry;
use App\Entity\Settings;
use App\Form\ContractType;
use App\Message\SmsNotification;
use App\Repository\ContractRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin/contract")
 * @IsGranted({"ROLE_ADD_ADMIN", "ROLE_MANAGE_BILLINGS"})
 */
class ContractController extends AbstractController
{
    /**
     * @Route("/", name="contract_index", methods={"GET"})
     */
    public function index(ContractRepository $contractRepository, MessageBusInterface $bus): Response
    {
        foreach ($contractRepository->findAll() as $driver) {
            $bus->dispatch(new SmsNotification($driver));
        }
//        $this->dispatchMessage(new SmsNotification('Look! I created a message!'));
//        dd($bus);

        return $this->render('contract/index.html.twig', [
            'contracts' => $contractRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="contract_new", methods={"GET","POST"})
     */
    public function newAction(Request $request): Response
    {
        $repository = $this->getDoctrine()->getRepository(RegistrationEntry::class);
        $id = $request->query->get('id');
        /** @var RegistrationEntry $entity */
        $contract = new Contract();
        if (null != $id) {
            $registrationEntity = $repository->find($id);
            $contract->setRegistrationId($registrationEntity->getId());
        }
        $form = $this->createForm(ContractType::class, $contract, ['action' => 'new']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            //  get number dogovor
            $contractRepository = $this->getDoctrine()->getRepository(Contract::class);
            $countYearNumber = $contractRepository->getCountDogovorsThisYear();
            $numberDogovor = $countYearNumber + 1;
            $numberDogovor .= '/'.date('y');
            $registrationEntity = $repository->find($form->getData()->getRegistrationId());
            $contract->setRegistration($registrationEntity);
//         var_dump($numberDogovor); die();
            $contract->setNumber($numberDogovor);
            $contract->setDate(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contract);
            $entityManager->flush();

            return $this->redirectToRoute('easyadmin', [
                'action' => 'list',
                'entity' => 'Contract',
            ]);
        }

        return $this->render('contract/new.html.twig', [
            'contract' => $contract,
            'form' => $form->createView(),
        ]);
        /*return $this->redirectToRoute('easyadmin', [
            'action' => 'new',
            'registrationEntityId' => $registrationEntity->getId(),
            'entity' => 'Contract'
        ]);
*/
    }

    /**
     * @Route("/{id}", name="contract_show", methods={"GET"})
     */
    public function show(Contract $contract): Response
    {
        return $this->render('contract/show.html.twig', [
            'contract' => $contract,
        ]);
    }

    /**
     * Create word file from datas driver
     * 
     * @todo Create handler
     * 
     * @Route("/{id}/word", name="contract_word", methods={"GET","POST"})
     */
    public function word(Request $request, Contract $contract): Response
    {
        $settingRepository = $this->getDoctrine()->getRepository(Settings::class);
        /** @var RegistrationEntry $entity */
        $settingEntity = $settingRepository->findOneBy(['name' => 'DOGOVOR_PATH']);
        $registration = $contract->getRegistration();
        $fileName = str_replace('/', '-', $contract->getNumber()).'-'.str_replace(' ', '-', $contract->getFullName()).'.docx';
        //$fileName =  $registration->getPhone() . '-' . $contract->getDateOfContractForFile() . '-'. md5($contract->getFullName()) . '.docx';
        $filePath = $settingEntity ?
                    rtrim(trim($settingEntity->getValue()), DIRECTORY_SEPARATOR)
                    : '.';
        $templateProcessor = new \PhpOffice\PhpWord\TemplateProcessor($filePath.DIRECTORY_SEPARATOR.'template'.DIRECTORY_SEPARATOR.'contract_template.docx');
        $templateProcessor->setValue('pasport_number', $contract->getPasportNumber());
        $templateProcessor->setValue('phone_number', $registration->getPhone());
        $templateProcessor->setValue('fullName', $contract->getFullName());
        $templateProcessor->setValue('SurnameNM', $contract->getShortName());
        $templateProcessor->setValue('number_of_contract', $contract->getNumber());
        $templateProcessor->setValue('date_of_issue', $contract->getDateOfIssue());
        $templateProcessor->setValue('date_of_birth', $contract->getDateOfBirth());
        $templateProcessor->setValue('date_of_contract', $contract->getDateOfContract());
        $templateProcessor->setValue('by_whom', $contract->getByWhom());
        $templateProcessor->setValue('address', $contract->getAddress());
        $templateProcessor->saveAs($filePath.DIRECTORY_SEPARATOR.$fileName);
        $fileResponse = new  BinaryFileResponse($filePath.DIRECTORY_SEPARATOR.$fileName);
        $fileResponse->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName
            );

        return $fileResponse;
    }

    /**
     * @Route("/{id}/edit", name="contract_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Contract $contract): Response
    {
        $form = $this->createForm(ContractType::class, $contract, ['action' => 'edit']);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('easyadmin', [
                'action' => 'list',
                'entity' => 'Contract',
            ]);
        }

        return $this->render('contract/edit.html.twig', [
            'entity' => $contract,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="contract_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Contract $contract): Response
    {
        if ($this->isCsrfTokenValid('delete'.$contract->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($contract);
            $entityManager->flush();
        }

        return $this->redirectToRoute('contract_index');
    }
}
