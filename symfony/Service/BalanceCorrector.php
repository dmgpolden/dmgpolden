<?php

namespace App\Service;

use Datetime;

class BalanceCorrector
{
    /** @var EntityManager */
    private $em;

    public function __construct($em, $user)
    {
        $this->em = $em;
        $this->user = $user;
    }

    public function alignmentDrivers(array $drivers)
    {
        $driversGood = [];
        $driversError = [];
        foreach ($drivers as $phone => $driver) {
            $registration = $this->getOrCreateRegistration($phone, $driver);
            if ($registration->getBalance() == $driver['balance']) {
                $driversGood[] = $phone.': Balance already correct';

                continue;
            }
            $res = $this->transaction($registration, $driver, \App\Entity\BillingTransaction::PROVIDER_ALIGNMENT);
            if (null == $res) {
                $driversError[] = $phone;
            } else {
                $driversGood[] = $res;
            }
        }

        return ['good' => $driversGood, 'error' => $driversError];
    }

    public function transaction(\App\Entity\RegistrationEntry $registrationEntry, array $driver, $provider)
    {
        $dGood = null;
        $dError = null;
        $ucfProvider = ucfirst(strtolower($provider));
        $remoteData = $ucfProvider.' '.$registrationEntry->getPhone().': '.implode(', ', $driver);
        $amount = $driver['balance'] - $registrationEntry->getBalance();
        $this->em->getConnection()->beginTransaction();

        try {
            $billingTransaction = new \App\Entity\BillingTransaction();
            $billingTransaction->setEntry($registrationEntry);
            //ask
            $billingTransaction->setProvider($provider);
            $billingTransaction->setType($amount > 0 ? \App\Entity\BillingTransaction::TYPE_CHECKIN : \App\Entity\BillingTransaction::TYPE_CHECKOUT);
            $billingTransaction->setSuccess(true);
            $billingTransaction->setCreatedBy($this->user->getEmail());
            $billingTransaction->setAt(new \DateTime('now'));
            $billingTransaction->setAmount($amount);
            $billingTransaction->setRemoteData($remoteData);
            $this->em->persist($billingTransaction);

            $registrationEntry->applyTransaction($billingTransaction);
            $registrationEntry->addToChronology("$ucfProvider balance from {$billingTransaction->getBalanceBefore()} to {$billingTransaction->getBalanceAfter()} with amount $amount");

            $this->em->persist($registrationEntry);

            $this->em->flush();
            $this->em->getConnection()->commit();
            $cardEnabled = $registrationEntry->getCardEnabled() ? 'Yes' : 'Not';
            $res = " {$registrationEntry->getPhone()}: $ucfProvider balance from {$billingTransaction->getBalanceBefore()} to {$billingTransaction->getBalanceAfter()} with amount $amount";
        } catch (\Exception $e) {
//            throw $e;
            $this->em->getConnection()->rollBack();
            $res = $registrationEntry->getPhone();
        }

        return $res;
    }

    public function getOrCreateRegistration($phone, $driver)
    {
        $registrationEntry = $this->em->getRepository(\App\Entity\RegistrationEntry::class)->findOneBy([
            'phone' => $phone,
        ]);
        if (!$registrationEntry) {
            $registrationEntriesNonExisting[] = $phone;
        }
        if (!$registrationEntry) {
            $registrationEntry = new \App\Entity\RegistrationEntry();
            $registrationEntry->setPhone($phone);
            $registrationEntry->setSms(rand(1001, 9999));
            $registrationEntry->setCreatedAt(new DateTime('now'));
            $registrationEntry->setAt(new DateTime('now'));
            $registrationEntry->setYesno(true);
            $registrationEntry->setChecked(true);
            $registrationEntry->setCardEnabled(false);
            $registrationEntry->setType($driver['direct'] ? \App\Entity\RegistrationEntry::TYPE_DIRECT : \App\Entity\RegistrationEntry::TYPE_URBAN);
            $registrationEntry->setDriverNameFirst($driver['nameF']);
            $registrationEntry->setDriverNameLast($driver['nameL']);
            $registrationEntry->setToken(uniqid().'_'.uniqid().'_'.uniqid());
            $registrationEntry->addToChronology('Added from import ');
        }
        $registrationEntry->setYesno(true);
        $registrationEntry->setChecked(true);
        $registrationEntry->setType($driver['direct'] ? \App\Entity\RegistrationEntry::TYPE_DIRECT : \App\Entity\RegistrationEntry::TYPE_URBAN);

        return $registrationEntry;
    }

    public function weekCorrect(\App\Entity\RegistrationEntry $registrationEntry, array $dItem, bool $wasImported)
    {
        $dU = [];
        $dP = [];
        $amount = floatval($dItem[1]);
        $remoteData = 'Import: '.implode(', ', $dItem);
        $dtNow = new \DateTime();
        $dateBegin = new \DateTime('today');
        dump($dateBegin);
        $cardEnabledWas = $registrationEntry->getCardEnabled() ? 'Yes' : 'Not';
        $lastMonday = new \DateTime('monday this week');
        $lastWeekTransaction = $this->em->getRepository(\App\Entity\BillingTransaction::class)->findEndForDay($registrationEntry, $lastMonday);
        $lastSunday = new \DateTime('monday this week - 1 second');
        if ($wasImported) {
            $dU[] = 'Correct skipped. Already done '.$remoteData;

            return [$dU, $dP];
        } else {
            $lastCorrectTransaction = $this->em->getRepository(\App\Entity\BillingTransaction::class)->findLastFromDateAfter($registrationEntry, $dateBegin, [
                [
                    'field' => 'provider',
                    'operator' => 'IN',
                    'parameter' => [\App\Entity\BillingTransaction::PROVIDER_CORRECT],
                ],
            ]);
            if (null != $lastCorrectTransaction) {
                if ($lastCorrectTransaction->getRemoteData() == $remoteData) {
                    $dU[] = 'Correct skipped. Already done '.$remoteData;

                    return [$dU, $dP];
                }

                $revertTransaction = new \App\Entity\BillingTransaction();
                $revertAmount = 0 - $lastCorrectTransaction->getAmount();
                $revertTransaction->setEntry($registrationEntry);
                $revertTransaction->setProvider(\App\Entity\BillingTransaction::PROVIDER_CORRECT);
                $revertTransaction->setType($revertAmount >= 0 ? \App\Entity\BillingTransaction::TYPE_CHECKIN : \App\Entity\BillingTransaction::TYPE_CHECKOUT);
                $revertTransaction->setSuccess(true);
                $revertTransaction->setCreatedBy($this->user->getEmail());
                $revertTransaction->setAt($lastSunday->modify('+1 millisecond'));
                $revertTransaction->setAmount($revertAmount);
                $revertTransaction->setRemoteData('Revert: '.$lastCorrectTransaction->getRemoteData());
                $this->em->persist($revertTransaction);

                $balance = null != $lastWeekTransaction ? $lastWeekTransaction->getBalanceAfter() : 0;
                $revertTransaction->setBalanceBefore($balance);
                switch ($revertTransaction->getType()) {
                    case \App\Entity\BillingTransaction::TYPE_CHECKOUT:
                        $balance -= abs(floatval($revertTransaction->getAmount()));
                        $registrationEntry->setBalance($registrationEntry->getBalance() - abs(floatval($revertAmount)));

                        break;
                    case \App\Entity\BillingTransaction::TYPE_CHECKIN:
                        $balance += abs(floatval($revertTransaction->getAmount()));
                        $registrationEntry->setBalance($registrationEntry->getBalance() + abs(floatval($revertAmount)));

                        break;
                }
                $revertTransaction->setBalanceAfter($balance);
                $this->em->persist($revertTransaction);
                $this->correctBalances($registrationEntry, $lastMonday, $revertAmount);
                $lastWeekTransaction = $this->em->getRepository(\App\Entity\BillingTransaction::class)->findEndForDay($registrationEntry, $lastMonday, [
                    [
                        'field' => 'provider',
                        'operator' => 'NOT IN',
                        'parameter' => [\App\Entity\BillingTransaction::PROVIDER_CORRECT],
                    ],
                ]);
            }
        }

        // dump($revertTransaction);
        // dump($lastWeekTransaction);
        $this->em->getConnection()->beginTransaction();

        try {
            $billingTransaction = new \App\Entity\BillingTransaction();
            $billingTransaction->setEntry($registrationEntry);
            $billingTransaction->setProvider(\App\Entity\BillingTransaction::PROVIDER_CORRECT);
            $billingTransaction->setType($amount >= 0 ? \App\Entity\BillingTransaction::TYPE_CHECKIN : \App\Entity\BillingTransaction::TYPE_CHECKOUT);
            $billingTransaction->setSuccess(true);
            $billingTransaction->setCreatedBy($this->user->getEmail());
            $billingTransaction->setAt($lastSunday);
            $billingTransaction->setAmount($amount);
            $billingTransaction->setRemoteData($remoteData);
            $this->em->persist($billingTransaction);

            $balance = null != $lastWeekTransaction ? $lastWeekTransaction->getBalanceAfter() : 0;

            $billingTransaction->setBalanceBefore($balance);
            switch ($billingTransaction->getType()) {
                case \App\Entity\BillingTransaction::TYPE_CHECKOUT:
                    $balance -= abs(floatval($billingTransaction->getAmount()));
                    $registrationEntry->setBalance($registrationEntry->getBalance() - abs(floatval($amount)));

                    break;
                case \App\Entity\BillingTransaction::TYPE_CHECKIN:
                    $balance += abs(floatval($billingTransaction->getAmount()));
                    $registrationEntry->setBalance($registrationEntry->getBalance() + abs(floatval($amount)));

                    break;
            }
            $billingTransaction->setBalanceAfter($balance);

            $cardEnabled = $registrationEntry->getCardEnabled() ? 'Yes' : 'Not';
            $registrationEntry->addToChronology('Correct from '.$lastMonday->format('d.m.Y').' : '.$registrationEntry->getPhone().' : '.$amount.' : cardEnabled = '.$cardEnabled);
            // dump($registrationEntry);
            $this->correctBalances($registrationEntry, $lastMonday, $amount);

            $this->em->persist($registrationEntry);

            $this->em->flush();
            $this->em->getConnection()->commit();
            $dU[] = 'Correct from '.$lastMonday->format('d.m.Y').' : '.$registrationEntry->getPhone().' : '.$amount.' : cardEnabled = '.$cardEnabled;
        } catch (\Exception $e) {
            throw new \Exception($e);
            $this->em->getConnection()->rollBack();
            $dP[] = $phone;
        }

        return [$dU, $dP];
    }

    public function correctBalances(\App\Entity\RegistrationEntry $registrationEntry, \DateTime $lastMonday, $amount)
    {
        $lastTransactions = $this->em->getRepository(\App\Entity\BillingTransaction::class)->findFromDate($registrationEntry, $lastMonday);
        // dump($lastTransactions);
        foreach ($lastTransactions as $lTrans) {
            $lTrans->setBalanceAfter($lTrans->getBalanceAfter() + floatval($amount));
            $lTrans->setBalanceBefore($lTrans->getBalanceBefore() + floatval($amount));
            // dump($lTrans);
            $this->em->persist($lTrans);
        }
        // dump('correctBalances');
    }
}
