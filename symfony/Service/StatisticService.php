<?php

namespace App\Service;

use App\Controller\BillingController;
use App\Entity\ImportHistory;
use App\Entity\Statistic;
use App\Entity\StatisticWeek;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use stdClass;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Finder\Finder;
use function dump;

/**
 * Сбор статистики на основе загруженных данных
 */

class StatisticService
{
    protected $em;
    protected $billC;
    protected $finder;
    protected $localpath;
    protected $bdPath;

    public function __construct(EntityManagerInterface $em, BillingController $billC, string $path)
    {
        $this->finder = new Finder();
        $this->em = $em;
        $this->path = $path;
        $this->bdPath = '/var/www/bolt-web/host/uploads/';
        $this->billC = $billC;
    }

    /**
     * Для консольного приложения
     * @param type $path
     * @param ProgressBar $progressBar
     * @return int
     */
    public function collect($path = null, ProgressBar $progressBar = null)
    {
        if (null !== $path) {
            $this->finder->files()->in($path);
            foreach ($this->finder as $importHistory) {
                $this->workerFile($importHistory->getPathname(), $importHistory->getFilename());
            }
            return 0;
        } else {
            $impHistoryRep = $this->em->getRepository(ImportHistory::class);
            foreach ($impHistoryRep->findAll() as $importHistory) {
                $fileImportHistory = str_replace($this->bdPath, $this->path.DIRECTORY_SEPARATOR.'host'.DIRECTORY_SEPARATOR.'uploads'.DIRECTORY_SEPARATOR, $importHistory->getFileName());
                $this->workerFile($fileImportHistory, $importHistory->getOriginal());
                
                if (null !== $progressBar){
                    $progressBar->advance();
                }
            }

            return 0;
        }
    }

    private function workerFile($importHistory, $fileName)
    {
        //dump($fileName); //importHistory->getOriginal()
        //dump($importHistory); //importHistory->getOriginal()
        if (preg_match('/((\d{4}W\d{1,2})|(\d{2}\.\d{2}.\d{4})).*(Urban).+?(Direct)?/', $fileName, $_name)) {
        //dump($_name);
            $urban = isset($_name[4]) && !isset($_name[5]) ? true : false;
            $direct = isset($_name[5]) && 'Direct' == $_name[5] ? true : false;
        } else {
            return 1; //  throw new \Exception("Error file name {$importHistory}");
        }

        try {
            $file = fopen($importHistory, 'r');
        } catch (Exception $e) {
//            throw $e;
            return 1;
        }
        unset($r_overall);
        unset($r_individual);
        unset($r_sum);
        list($r_overall, $r_individual, $r_sum) = $this->billC->extractData($file);
        if (!empty($_name[2])) { // Weekly file
            $preDataWeek = $this->prepareWeek($r_overall, $r_individual, $r_sum, $urban, $direct);
            if ($preDataWeek) {
                $this->createRowWeek($preDataWeek);
            }
        }
        if (!empty($_name[3])) { // Daily file
            $preDataDay = $this->prepareDay($r_overall, $r_individual, $r_sum, $urban, $direct);
            if ($preDataDay) {
                $this->createRowDay($preDataDay);
            }
        }
    }

    public function createRowWeek(stdClass $data)
    {
        $statisticRep = $this->em->getRepository(StatisticWeek::class);
        
        /**
         * Проверяем наличие данных
         */
        $statistic = $statisticRep->findOneBy(['dateEnd' => $data->dateEnd, 'direct' => $data->direct]);
        
        if (null == $statistic) {
            $statistic = new StatisticWeek();
        }
        $statistic->setDateBegin($data->dateBegin);
        $statistic->setDateEnd($data->dateEnd);
        $statistic->setDirect($data->direct);
        $statistic->setSumPays($data->sumPays);
        $statistic->setSumBonuses($data->sumBonuses);
        $statistic->setSumOrders($data->sumOrders);
        $statistic->setSumDrivers($data->sumDrivers);
        $statistic->setComission($data->comission);
        $statistic->setComissionSum($data->comissionSum);
        $statistic->setSumZ($data->sumZ);
        $statistic->setSumToPays($data->sumToPays);
        $this->em->persist($statistic);
        $this->em->flush();

        return $statistic;
    }

    public function createRowDay(stdClass $data)
    {
        $statisticRep = $this->em->getRepository(Statistic::class);
        $statistic = $statisticRep->findOneBy(['date' => $data->date, 'direct' => $data->direct]);
        if (null == $statistic) {
            $statistic = new Statistic();
        }
        $statistic->setDate($data->date);
        $statistic->setDirect($data->direct);
        $statistic->setSumOrders($data->sumOrders);
        $statistic->setSumDrivers($data->sumDrivers);
        $statistic->setCountOrderDrivers($data->countOrderDrivers);
        $statistic->setCountDrivers($data->countDrivers);
        $statistic->setCountOrders($data->countOrders);
        $statistic->setComission($data->comission);
        $statistic->setComissionSum($data->comissionSum);
        $statistic->setCountOrdersDivideCountOrderDrivers($data->countOrdersDivideCountOrderDrivers);
        $statistic->setSumOrdersDivideCountOrders($data->sumOrdersDivideCountOrders);
        $this->em->persist($statistic);
        $this->em->flush();

        return $statistic;
    }

    /**
     * Get Comission
     * @param bool $urban
     * @param bool $direct
     * @return float 
     * @throws Exception
     */
    public function getComission($urban, $direct)
    {
        $conn = $this->em->getConnection();
        $comission = 0;
        if ($urban) {
            $sqlUrban = "
                SELECT substring(name from 18 for 10)::date as date, value FROM settings
                    WHERE name like 'commission_Urban_%' 
                    AND substring(name from 18 for 10)::date < NOW()::date
                    ORDER BY date DESC LIMIT 1
                ";
            $stmtUrban = $conn->prepare($sqlUrban);
            $stmtUrban->execute();
            $comissionUrban = $stmtUrban->fetchAll();
            if (empty($comissionUrban)) {
                throw new Exception('You must set comission as commission_Urban_2020-03-24');
            }
            $comission = $comissionUrban[0]['value'];
        }
        if ($direct) {
            $sqlDirect = "
                SELECT substring(name from 19 for 10)::date as date, value FROM settings
                    WHERE name like 'commission_Direct_%' 
                    AND substring(name from 19 for 10)::date < NOW()::date
                    ORDER BY date DESC LIMIT 1
                ";
            $stmtDirect = $conn->prepare($sqlDirect);
            $stmtDirect->execute();
            $comissionDirect = $stmtDirect->fetchAll();
            if (empty($comissionDirect)) {
                throw new Exception('You must set comission as commission_Direct_2020-03-24');
            }
            $comission = $comissionDirect[0]['value'];
        }
        if (strpos($comission, '%') > 0) {
            $comission = (float) str_replace('%', '', $comission) / 100;
        }

        return (float) str_replace(',', '.', $comission);
    }

    public function prepareWeek($r_overall, $r_individual, $r_sum, $urban, $direct)
    {
        if ($urban == $direct) {
            throw new Exception('I don`t undastand Urban or Direct');
        }
        $r = new stdClass();
        $r->direct = $direct;
        if (isset(explode(' ', trim($r_sum[2]))[1])) {
            $r->dateBegin = DateTime::createFromFormat('Y-m-d H:i:s',
            explode(' ', trim($r_sum[2]))[1].' 00:00:00'
            );
        }
        if (isset(explode(' ', trim($r_sum[2]))[3])) {
            $r->dateEnd = DateTime::createFromFormat('Y-m-d H:i:s',
            explode(' ', trim($r_sum[2]))[3].' 23:59:59'
            );
        }
        if (!$r->dateBegin || (!$r->dateEnd)) {
            dump('Unable to parse date '.trim($r_sum[2]));
        }
        $r->sumPays = 0; //Перечисленные суммы (X)
       $r->sumOrders = (float) $r_sum[3]; //Сумма всех заказов - это колонка D2
       $r->sumBonuses = (float) $r_sum[11]; // ССумма бонусов водителей (L2) -  просто тут заполняем значение из L2
       $r->sumDrivers = (float) $r_sum[count($r_sum) - 1]; // Суммы начисленные водителям - Это колонка N2
       $r->comission = $this->getComission($urban, $direct);
        $r->comissionSum = $r->sumOrders * $r->comission;
        //Сумма бонусов водителей /0,95*0,05 (Z) - тут значение из колонки L2/0.95*0.05 (L2 делим на 0,95 и умножаем 0,05)
        $r->sumZ = $r->sumBonuses / 0.95 * 0.05;
        $r->sumToPays = 0; // $r->sumPays - $previous->getSumPays() - $previous->getSumZ() ;

        //Сумма для оплаты водителям - Тут формула такая - X-Y(предыдущего периода) - Z(предыдущего периода)
        return $r;
    }

    public function prepareDay($r_overall, $r_individual, $r_sum, $urban, $direct)
    {
        if ($urban == $direct) {
            throw new Exception('I don`t undastand Urban or Direct');
        }
        $r = new stdClass();
        $r->direct = $direct;
        if (isset(explode(' ', trim($r_sum[2]))[3])) {
            $r->date = DateTime::createFromFormat('Y-m-d H:i:s',
                explode(' ', trim($r_sum[2]))[3].' 23:59:59'
                );
        } else {
            $r->date = DateTime::createFromFormat('Y-m-d', explode(' ', trim($r_sum[2]))[1]);
        }
        if (!$r->date) {
            dump('Unable to parse date '.trim($r_sum[2]));
        }
        $r->sumOrders = (float) $r_sum[3]; //Сумма всех заказов - это колонка D2 Суммы начисленные водителям
       $r->sumDrivers = (float) $r_sum[count($r_sum) - 1]; // Суммы начисленные водителям - Это колонка N2
       $r->countOrderDrivers = (int) count($this->array_unique_key($r_individual, '1')); //Ядро - это количевтсо водителей, которые сделали хоть один заказ за этот период. То есть это количество уникальных телефонных номеров из строк 1158-2135
       $r->countDrivers = (int) count($r_overall); //Количество водителей - это количество строк 3-1155
       $r->countOrders = (int) count($r_individual); // Количество заказов
       if (0 == $r->countOrders || 0 == $r->countDrivers) {
           return false;
       }
        $r->comission = (float) $this->getComission($urban, $direct);
        $r->comissionSum = (float) $r->sumOrders * $r->comission;
        $r->countOrdersDivideCountOrderDrivers = (float) $r->countOrders / $r->countOrderDrivers; //Количество/водитель - это количество заказов делим на Ядро, к примеру E20/H20
       $r->sumOrdersDivideCountOrders = (float) $r->sumOrders / $r->countOrders; // Средний чек - Общую сумму заказов делим на общее количество заказов, к примеру L20/E20
    return $r;
    }

    public function array_unique_key($array, $key)
    {
        $tmp = $key_array = [];
        $i = 0;

        foreach ($array as $val) {
            if (!in_array($val[$key], $key_array)) {
                $key_array[$i] = $val[$key];
                $tmp[$i] = $val;
            }
            ++$i;
        }

        return $tmp;
    }
}
